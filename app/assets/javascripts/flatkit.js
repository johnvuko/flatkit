//= require flatkit/libs/jquery/jquery/dist/jquery.js
//= require flatkit/libs/jquery/tether/dist/js/tether.min.js
//= require flatkit/libs/jquery/bootstrap/dist/js/bootstrap.js
//= require flatkit/libs/jquery/underscore/underscore-min.js
//= require flatkit/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js
//= require flatkit/libs/jquery/PACE/pace.min.js

//= require flatkit/scripts/config.lazyload.js
//= require flatkit/scripts/palette.js
//= require flatkit/scripts/ui-load.js
//= require flatkit/scripts/ui-jp.js
//= require flatkit/scripts/ui-include.js
//= require flatkit/scripts/ui-device.js
//= require flatkit/scripts/ui-form.js
//= require flatkit/scripts/ui-nav.js
//= require flatkit/scripts/ui-screenfull.js
//= require flatkit/scripts/ui-scroll-to.js
//= require flatkit/scripts/ui-toggle-class.js

//= require flatkit/scripts/app.js

// require flatkit/libs/jquery/jquery-pjax/jquery.pjax.js
//= require flatkit/scripts/ajax.js

//= require cocoon
//= require_self

$(function(){

	/* for tab_links */
	$(document).on('cocoon:before-insert', function(e, item) {
		var coccon_class = $(item).find('[data-cocoon-class]').attr('data-cocoon-class');
		if(!coccon_class){
			return;
		}
		
		var randomValue = Math.floor(Math.random() * 10000000).toString();

		$(item).find('a[data-target]').each(function(i, element){
			var target = $(item).find($(element).attr('data-target'));

			$(element).attr('data-target', $(element).attr('data-target') + randomValue);
			target.attr('id', target.attr('id') + randomValue);
		});
	});

	/* sort table element */
	$('tr.list-group-item').on('dragend', function(){
		var container = $(this).parents('.table-responsive');
		var form = container.find('form.js-jt-sortable');

		var ids = $.map(container.find('[data-jt-id]'), function(element, _) {
			return $(element).attr('data-jt-id');
		});

		form.find('input#position_ids').val(ids);
		form.submit();
	});

	/* sort table element nestable */
	$('table.dd').on('change', function(){
		var container = $(this).parents('.table-responsive');
		var form = container.find('form.js-jt-sortable');

		var ids = $.map(container.find('[data-jt-id]'), function(element, _) {
			return $(element).attr('data-jt-id');
		});

		form.find('input#position_ids').val(ids);
		form.submit();
	});

	// Switch
	$(document).on('change', 'form.js-submit-on-change input', function(){
		$(this).parents('form').first().submit();
	});

	$(document).on('change', 'form.js-submit-on-change select', function(){
		$(this).parents('form').first().submit();
	});

	$('.form-group.file[data-media-url]').each(function(){
		var container = $('<div></div>');
		$(this).append(container);
		container.addClass('form-control');

		var thumb = $('<span class="w-80 m-r"><img></span>');
		thumb.find('img').attr('src', $(this).attr('data-media-url')).addClass('w-80');
		thumb.appendTo(container);

		var input = $(this).find('input');
		input.removeClass('form-control');
		input.appendTo(container);
	});

});

$(function(){
	if($('.navside').length == 0){
		return;
	}

	var slice = 3;

	var path = document.location.pathname.split('/').slice(0, slice).join('/');
	var li = $('.navside ul.nav li a[href="' + path + '"]').parent();
	if(li.length != 1){
		li = $('.navside ul.nav li a[href^="' + path + '"]').parent();
	}
	slice++;

	while(li.length > 1){
		path = document.location.pathname.split('/').slice(0, slice).join('/');
		li = $('.navside ul.nav li a[href="' + path + '"]').parent();
		if(li.length != 1){
			li = $('.navside ul.nav li a[href^="' + path + '"]').parent();
		}
		
		slice++;
	}

	if(li.length == 0){
		return;
	}
	
	li.addClass('selected');
	var liContainer = li.parents('.nav-sub.nav-mega');
	if(liContainer){
		// open the container
		liContainer.parent().addClass('active');
	}
});
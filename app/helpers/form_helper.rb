module FormHelper

  def form_resource(title, object, url)
    form_wrapper title do
      simple_form_for object, url: url do |f|
        render 'form', f: f
      end
    end
  end

  def form_wrapper(title, &block)
    builder = FormWrapper.new(title, &block)
    builder.html
  end

  class FormWrapper

    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::UrlHelper
    include ActionView::Helpers::TextHelper
    include ActionView::Context

    attr_reader :title, :block

    def initialize(title, &block)
      @title = title
      @block = block
    end

    def html
      content_tag :div, class: :padding do
        content_tag :div, class: :box do

          if !@title.blank?
            concat(content_tag(:div, class: 'box-header light') do
              content_tag :h2, @title
            end)
          end

          concat(content_tag(:div, class: 'box-body') do
            @block.call
          end)

        end
      end
    end

  end

end

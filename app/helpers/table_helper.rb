module TableHelper

  def table(collection = [], options = {})
    options[:sortable_url] = url_for({action: :update_position}) if options[:sortable]

    builder = TableBuilder.new(collection, options)
    yield(builder) if block_given?

    builder.html
  end

  class TableBuilder

    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::UrlHelper
    include ActionView::Helpers::TextHelper
    include ActionView::Helpers::FormTagHelper
    include ActionView::Context

    attr_reader :collection, :options, :headers, :columns

    def initialize(collection, options = {})
      @title = options[:title]
      @collection = collection
      @options = options
      @headers = []
      @columns = []

      @before = nil
      @after = nil
    end

    def collection(&block)
      return if !block_given?

      @create_header = true
      @column_id = 0

      for object in @collection
        @current_object = object
        yield(object) 

        @create_header = false
        @column_id = 0
      end

      @create_header = nil
      @column_id = nil
      @current_object = nil
    end

    def link(*args, &block)
      if @current_object != nil
        link_in_collection(*args, &block)
      else
        link_general(*args, &block)
      end
    end

    def col(attribute = nil, options = {}, &block)
      @headers.push({
        title: ((options[:header] || {})[:title] || translate(attribute)),
        options_header: options[:header] || {},
        options_content: options[:content] || {},
      })

      new_col = []
      @columns.push(new_col)

      for object in @collection
        if block_given?
          new_col.push yield(object)
        else
          new_col.push object.send(attribute)
        end
      end
    end

    def before(&block)
      return if !block_given?
      @before = block
    end

    def after(&block)
      return if !block_given?
      @after = block
    end

    def html
      content_tag :div, class: :padding do
        content_tag :div, class: :box do

          if !@title.blank?
            concat(content_tag(:div, class: 'box-header light') do
              content_tag :h2, @title
            end)
          end

          if @before
            concat(content_tag(:div, class: 'box-body') do
              @before.call
            end)
          end

          concat(content_tag(:div, class: 'table-responsive') do

            table_options = { class: 'table table-striped b-t b-b' }
            tbody_options = {}
            tr_options = {}

           if options[:sortable]
              concat(form_tag(options[:sortable_url], class: 'js-jt-sortable', remote: true) do
                concat(hidden_field_tag :position_ids, @collection.map(&:id).join(','))
              end)

              table_options[:class] += " dd"
              table_options['ui-jp'] = 'nestable'
              table_options['ui-options'] = "{listNodeName: 'tbody', itemNodeName: 'tr', maxDepth:1}"

              tbody_options = {
                class: 'dd-list',
              }

              tr_options = {
                class: 'dd-item'
              }
            end
            
            concat(content_tag(:table, table_options) do

              unless @options[:header] === false
                concat(content_tag(:thead) do
                  content_tag :tr do
                    concat(content_tag(:th, nil, style: 'width: 1px;')) if options[:sortable]
                    for header in @headers
                      concat content_tag(:th, header[:title], header[:options_header])
                    end
                  end
                end)
              end

              concat(content_tag(:tbody, tbody_options) do
                @collection.each_with_index do |object, i|
                  concat(content_tag(:tr, tr_options.merge({'data-jt-id': object.id})) do

                    if options[:sortable]
                      concat(content_tag(:td, class: 'dd-handle js-handle') do
                        content_tag(:i, nil, class: 'fa fa-reorder')
                      end)
                    end
                    @columns.each_with_index do |col, j|
                      value = col[i]
                      concat content_tag(:td, value, @headers[j][:options_content])
                    end
                  end)
                end
              end)
            end)
          end)

          if @after
            concat(content_tag(:div, class: 'box-body') do
              @after.call
            end)
          end

        end
      end
    end

    private

    def link_in_collection(attribute, url, options = {}, &block)
      if @create_header
        @headers.push({
          title: (options[:header] || {})[:title] || (attribute.is_a?(Symbol) ? translate(attribute) : ""),
          options_header: options[:header] || {},
          options_content: options[:content] || {},
          })				
      end

      new_col = @columns[@column_id]
      if !new_col
        new_col = []
        @columns.push(new_col)
      end
      @column_id += 1

      link_title = attribute.is_a?(Symbol) ? @current_object.send(attribute) : attribute
      new_col.push link_to(link_title || "", url, options[:link])
    end

    def link_general(attribute, options = {}, &block)
      @headers.push({
        title: (options[:header] || {})[:title] || translate(attribute),
        options_header: options[:header] || {},
        options_content: options[:content] || {},
        })

      new_col = []
      @columns.push(new_col)

      for object in @collection
        link_title = attribute.is_a?(Symbol) ? object.send(attribute) : attribute
        link_content = link_to(yield(object), options[:link]) do
          link_title
        end
        new_col.push link_content
      end
    end

    def translate(attribute)
      return ''.freeze if attribute.blank?

      if @options[:model]
        @options[:model].human_attribute_name(attribute)
      else
        attribute.to_s.try(:titleize)
      end
    end

  end

end

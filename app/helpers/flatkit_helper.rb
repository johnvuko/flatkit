module FlatkitHelper

  def side_menu_section(title)
    content_tag :li, class: 'nav-header hidden-folded' do
      content_tag :small, title, class: 'text-muted'
    end
  end

  # options = { logo = URL, badge: String }
  def side_menu_link(title, url = '#', options = {}, &block)
    content_tag :li do
      link_to url do
        if block_given?
          concat(content_tag(:span, class: 'nav-caret') do
            content_tag :i, nil, class: "fa fa-caret-down"
          end)
        end
        if options[:badge]
          concat(content_tag(:span, class: 'nav-label') do
            content_tag :b, options[:badge], class: "label rounded label-sm primary"
          end)
        end
        if options[:logo]
          concat(content_tag(:span, class: 'nav-icon') do
            content_tag :i, nil, class: "fa #{options[:logo]}"
          end)
        end

        concat(content_tag(:span, title, class: 'nav-text'))

        if block_given?
          concat(content_tag(:ul, class: 'nav-sub nav-mega') do
            yield
          end)
        end
      end
    end
  end

  def link_to_edit(url)
    link_to ''.freeze, url, class: 'fa fa-pencil btn btn-sm white'
  end

  def link_to_destroy(url, css_class = nil)
    link_to ''.freeze, url, class: "fa fa-trash btn btn-sm danger #{css_class}", method: :delete, data: { confirm: t('common.messages.delete') }
  end

  def link_to_impersonate(url)
    link_to ''.freeze, url, class: 'fa fa-bolt btn btn-sm white', style: "width: 30px; height: 27px;", data: { confirm: t('common.messages.impersonate_user') }
  end

  def image_tag_thumb(image_url, options = {})
    size = options[:size] || 40
    content_tag :span, class: "w-#{size}" do
      image_tag image_url, class: "w-#{size}"
    end
  end

  def tabs_links(links = [], options = {})
    div_options = {
      class: 'b-b b-primary nav-active-primary'
    }

    div_options['data-cocoon-class'] = options[:cocoon_class] if options[:cocoon_class]

    prefix = options.fetch(:prefix, '')

    content_tag :div, div_options do
      content_tag :ul, class: 'nav nav-tabs' do
        links.each do |text, id|
          concat(content_tag(:li, class: 'nav-item') do
            if id == links[0][1]
              link_to text, '#', class: 'nav-link active', 'data-toggle': 'tab', 'data-target': "#tab-#{id}#{prefix}", 'aria-expanded': 'true'
            else
              link_to text, '#', class: 'nav-link', 'data-toggle': 'tab', 'data-target': "#tab-#{id}#{prefix}", 'aria-expanded': 'false'
            end
          end)
        end
      end
    end
  end

  def tabs_contents(objects = [], options = {})
    div_options = {
      class: 'tab-content p-a m-b-md'
    }

    div_options['data-cocoon-class'] = options[:cocoon_class] if options[:cocoon_class]

    prefix = options.fetch(:prefix, '')

    content_tag :div, div_options do
      objects.each do |object, id|
        concat(content_tag(:div, class: "tab-pane animated fadeIn text-muted #{id == objects[0][1] ? 'active' : ''}", id: "tab-#{id}#{prefix}") do
          yield(object) if block_given?
        end)
      end
    end
  end

  def print_boolean(value)
    return t('common.yes') if value == true
    return t('common.no') if value == false
    ''
  end

end

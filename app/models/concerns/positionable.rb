module Concerns::Positionable
	extend ActiveSupport::Concern

	included do

    validates :position, presence: true
    # validate :validates_position

    default_scope { order(position: :asc) }

    before_validation :set_default_position, on: :create

    def set_default_position
      self.position ||= (self.position_scope.maximum(:position) || 0) + 1
    end

    def validates_position
      errors.add(:position, :taken) if self.position_scope.where(position: position).count > 0
    end

    def position_scope
      self.class
    end

	end

end
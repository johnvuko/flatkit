module ResourcesController
  extend ActiveSupport::Concern

  def index
    @objects = collection_index
  end

  def show
    @object = collection.find(params[:id])
  end

  def new
    @object = collection.new
  end

  def create
    @object = collection.new(object_params_create)
    if @object.save
      redirect_to action: :index if !request.xhr?
    else
      render :new if !request.xhr?
    end
  end

  def edit
    @object = collection.find(params[:id])
  end

  def update
    @object = collection.find(params[:id])
    if @object.update(object_params_update)
      redirect_to action: :index if !request.xhr?
    else
      render :edit if !request.xhr?
    end
  end

  def destroy
    collection.destroy(params[:id])

    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  def update_position
    position_ids = params[:position_ids].split(',').map(&:to_i)
    collection.find(position_ids).sort_by {| object| position_ids.index(object.id) }.each_with_index {|object, i| object.update_column(:position, i)}

    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

protected

  def collection
    raise NotImplementedError
  end

  def collection_index
    collection
  end

  def object_params
    raise NotImplementedError
  end

  def object_params_create
    object_params
  end

  def object_params_update
    object_params
  end

end
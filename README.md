# Flatkit

## Installation

Include the gem in your Gemfile:

    gem 'flatkit'


Add the following to your application.js:

```
//= require flatkit
```

Also add to your application.css:

```
/*
 *= require flatkit
 */
```

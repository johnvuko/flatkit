module SimpleForm
  module Inputs
    class BooleanInput < Base

      def initialize(builder, attribute_name, column, input_type, options = {})
        options[:inline_label] = true
        options[:label] = false
        super(builder, attribute_name, column, input_type, options)
      end

      def input(wrapper_options = nil)
        merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)

        html_options = label_html_options.dup
        html_options[:class] ||= []
        html_options[:class].push(boolean_label_class) if boolean_label_class

        if nested_boolean_style?
          build_hidden_field_for_checkbox +
            @builder.label(label_target, html_options) {
              build_check_box_without_hidden_field(merged_input_options) +
                inline_label
            }
        else
          build_check_box(unchecked_value, merged_input_options)
        end
      end

      def inline_label
        inline_option = options[:inline_label]

        if inline_option
          label = inline_option == true ? label_text : html_escape(inline_option)
          "<i></i> #{label}".html_safe
        end
      end

    end
  end
end
Gem::Specification.new do |s|
	s.name = 'flatkit'
	s.summary = "Flatkit packaged for the Rails asset pipeline"
	s.description = "Flatkit's JavaScript, CSS, and image files packaged for the Rails asset pipeline"
	s.version = '1.1.2'
	s.files = `git ls-files`.split("\n")
	s.require_paths = ['lib']
	s.authors = "Jonathan Tribouharet"
	s.email = 'jonathan.tribouharet@gmail.com'
	s.license = 'MIT'
	s.platform = Gem::Platform::RUBY

	s.add_dependency 'simple_form'
	s.add_dependency 'cocoon'
	s.add_dependency 'kaminari'
end